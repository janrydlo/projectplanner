import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import MainNavigator from './navigation/MainNavigator';
import store from './store';

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Provider store={store}>
          <MainNavigator />
        </Provider>
      </View>
    );
  }
}
