import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { INSIGHTS_DATA_FETCH_FAIL, INSIGHTS_DATA_FETCH_SUCCESS } from './types';

import * as predictionLogic from '../logic';

export const getInsightsForPage = pageId => async dispatch => {
  const token = await AsyncStorage.getItem('fb_token');
  if (token) {
    doInsightsCall(pageId, token, dispatch);
  }
  dispatch({ type: INSIGHTS_DATA_FETCH_FAIL, payload: 'Uzivatel neni prihlasen' });
};

const doInsightsCall = async (pageId, token, dispatch) => {
  try {
    const { data: { access_token: pageToken } } = await axios.get(
      `https://graph.facebook.com/${pageId}?fields=access_token&access_token=${token}`
    );
    const insightsData = await axios.get(
      `https://graph.facebook.com/${pageId}/insights/page_fans_online,page_fans_gender_age,page_fans_country?date_preset=this_month&access_token=${pageToken}`
    );
    const today = new Date()
      .toJSON()
      .slice(0, 10)
      .replace(/-/g, '/');
    const todaysPosts = await axios.get(
      `https://graph.facebook.com/${pageId}/feed?since=${today}&access_token=${pageToken}`
    );
    const postsData = await axios.get(
      `https://graph.facebook.com/${pageId}/posts/?fields=created_time,insights.metric(post_impressions_unique),comments.limit(1).summary(total_count),shares,type,message,reactions.limit(0).summary(1).as(reactions)&access_token=${pageToken}`
    );
    const predictedTimes = predictionLogic.getBestDaysForPosts(
      insightsData,
      postsData,
      todaysPosts
    );
    dispatch({ type: INSIGHTS_DATA_FETCH_SUCCESS, payload: predictedTimes });
  } catch (error) {
    dispatch({ type: INSIGHTS_DATA_FETCH_FAIL, payload: error });
  }
};
