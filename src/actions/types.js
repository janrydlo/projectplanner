export const FACEBOOK_LOGIN_SUCCESS = 'facebook_login_success';
export const FACEBOOK_LOGIN_FAIL = 'facebook_login_fail';
export const PAGES_FETCH_SUCCESS = 'pages_fetch_success';
export const FETCH_FAIL = 'pages_fetch_fail';
export const PAGE_DATA_FETCH_SUCCESS = 'page_data_fetch_success';
export const TIMES_COUNTING_SUCCESS = 'times_counting_success';
export const INSIGHTS_DATA_FETCH_FAIL = 'insights_data_fetch_fail';
export const INSIGHTS_DATA_FETCH_SUCCESS = 'insights_data_fetch_success';
