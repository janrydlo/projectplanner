import { AsyncStorage } from 'react-native';
import axios from 'axios';

import {
  PAGES_FETCH_SUCCESS,
  FETCH_FAIL,
  PAGE_DATA_FETCH_SUCCESS,
  TIMES_COUNTING_SUCCESS
} from './types';

export const fetchPagesOfUser = () => async dispatch => {
  const token = await AsyncStorage.getItem('fb_token');
  if (token) {
    doFetchPages(token, dispatch);
  }
  dispatch({ type: FETCH_FAIL, payload: 'Uzivatel neni prihlasen' });
};

const doFetchPages = async (token, dispatch) => {
  try {
    const data = await axios.get(`https://graph.facebook.com/me/accounts?access_token=${token}`);
    const mappedData = await Promise.all(
      data.data.data.map(
        async (i): Promise<number> => await getPageDataObject(i.id, i.access_token)
      )
    );
    console.log(mappedData);
    dispatch({ type: PAGES_FETCH_SUCCESS, payload: mappedData });
  } catch (error) {
    dispatch({ type: FETCH_FAIL, payload: error });
  }
};

export const fetchPageData = (pageId, callback) => async dispatch => {
  if (pageId) {
    const token = await AsyncStorage.getItem('fb_token');
    if (token) {
      doFetchPageData(pageId, token, dispatch, callback);
    } else {
      dispatch({ type: FETCH_FAIL, payload: 'Uzivatel neni prihlasen' });
    }
  }
  dispatch({ type: FETCH_FAIL, payload: 'Nepovedlo se dotahnout data o strance' });
};

const doFetchPageData = async (pageId, token, dispatch, callback) => {
  try {
    const data = await getPageDataObject(pageId, token);
    dispatch({ type: PAGE_DATA_FETCH_SUCCESS, payload: data });
    callback();
  } catch (error) {
    dispatch({ type: FETCH_FAIL, payload: 'Nepodarilo se ziskat data o strance' });
  }
};

const getUrlOfPictureInRightSize = async (pageId, token) => {
  const picture = await axios.get(
    `https://graph.facebook.com/${pageId}/picture?access_token=${token}&type=normal&redirect=false`
  );
  return picture.data.data.url;
};

const getPageDataObject = async (pageId, token) => {
  const pageData = await axios.get(
    `https://graph.facebook.com/${pageId}?access_token=${token}&fields=about,picture,cover,general_info,name,engagement,website`
  );
  pageData.picture = await getUrlOfPictureInRightSize(pageId, token);
  return pageData;
};

export const findBestTimesToPost = () => ({
  type: TIMES_COUNTING_SUCCESS,
  payload: ['18:00', '19:00', '20:00']
});
