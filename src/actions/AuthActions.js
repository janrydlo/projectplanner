import { AsyncStorage } from 'react-native';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import _ from 'lodash';

import { FACEBOOK_LOGIN_SUCCESS, FACEBOOK_LOGIN_FAIL } from './types';
import { WANTED_FB_PERMISSION } from '../constants';

export const logInWithFacebook = () => async dispatch => {
  const token = await AsyncStorage.getItem('fb_token');

  if (token) {
    dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token });
  } else {
    doFacebookLogin(dispatch);
  }
};

const doFacebookLogin = async dispatch => {
  const { isCancelled, grantedPermissions } = await LoginManager.logInWithReadPermissions([
    WANTED_FB_PERMISSION
  ]);

  if (!isLoginSuccessful(isCancelled, grantedPermissions)) {
    return dispatch({ type: FACEBOOK_LOGIN_FAIL });
  }

  const { accessToken } = await AccessToken.getCurrentAccessToken();
  await AsyncStorage.setItem('fb_token', accessToken);
  dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: accessToken });
};

const isLoginSuccessful = (isCancelled, grantedPermissions) =>
  !isCancelled && _.includes(grantedPermissions, WANTED_FB_PERMISSION);
