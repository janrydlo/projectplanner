import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../actions';

class AuthScreen extends Component {
  componentDidMount() {
    this._sub = this.props.navigation.addListener('didFocus', () => {
      this.props.logInWithFacebook();
      this.onAuthComplete(this.props);
    });
  }

  componentWillReceiveProps(nextProps) {
    this.onAuthComplete(nextProps);
  }

  onAuthComplete(props) {
    if (props.token) {
      this.props.navigation.navigate('pages');
    }
  }

  render() {
    return <View />;
  }
}

function mapStateToProps({ AuthReducer }) {
  return { token: AuthReducer.token };
}

export default connect(mapStateToProps, actions)(AuthScreen);
