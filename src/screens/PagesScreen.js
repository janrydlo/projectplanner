import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { Header } from 'react-native-elements';

import * as actions from '../actions';

import PageList from '../components/PageList';

class PagesScreen extends Component {
  static navigationOptions = {
    title: 'My Pages'
  };

  componentDidMount() {
    this._sub = this.props.navigation.addListener('didFocus', () => {
      this.props.fetchPagesOfUser();
    });
  }

  showPageDetail = pageId => {
    this.props.fetchPageData(pageId, () => this.props.navigation.navigate('analytics'));
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header
          centerComponent={{
            text: 'Moje spravované stránky',
            style: { color: '#fff' }
          }}
          outerContainerStyles={{ backgroundColor: '#673AB7' }}
        />
        <PageList pages={this.props.pages} callback={this.showPageDetail} />
      </View>
    );
  }
}

function mapStateToProps({ PagesReducer }) {
  return { pages: PagesReducer.pages, error: PagesReducer.error };
}

export default connect(mapStateToProps, actions)(PagesScreen);
