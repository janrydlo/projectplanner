import React, { Component } from 'react';
import { View, AsyncStorage } from 'react-native';
import { SocialIcon, Avatar } from 'react-native-elements';
import { connect } from 'react-redux';
import * as actions from '../actions';

class WelcomeScreen extends Component {
  async componentWillMount() {
    const token = await AsyncStorage.getItem('fb_token');
    //await AsyncStorage.removeItem('fb_token');
    if (token) {
      this.props.navigation.navigate('pages');
    }
  }

  doFacebookLogin = () => {
    this.props.navigation.navigate('auth');
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.avatar}>
          <Avatar
            xlarge
            rounded
            source={{ uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg' }}
          />
        </View>
        <View style={styles.buttons}>
          <SocialIcon
            title="Sign In With Facebook"
            button
            type="facebook"
            raised
            onPress={this.doFacebookLogin}
          />
        </View>
      </View>
    );
  }
}

function mapStateToProps({ AuthReducer }) {
  return { token: AuthReducer.token, error: AuthReducer.error };
}

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#673AB7'
  },
  avatar: {
    alignItems: 'center'
  },
  buttons: {
    alignItems: 'stretch'
  }
};

export default connect(mapStateToProps, actions)(WelcomeScreen);
