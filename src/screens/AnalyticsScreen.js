import React, { Component } from 'react';
import { View } from 'react-native';
import { Header, Button } from 'react-native-elements';
import { connect } from 'react-redux';
import * as actions from '../actions';

import PageDetail from '../components/PageDetail';
import TimeList from '../components/TimeList';

class AnalyticsScreen extends Component {
  static navigationOptions = {
    title: 'Page Detail'
  };

  componentDidMount() {
    this._sub = this.props.navigation.addListener('didFocus', () => {
      this.props.getInsightsForPage(this.props.pageData.data.id);
      this.props.findBestTimesToPost();
    });
  }

  renderContent() {
    const { containerStyle } = styles;

    return (
      <View style={containerStyle}>
        <PageDetail data={this.props.pageData} />
        <TimeList data={this.props.insightsData} />
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Header
          leftComponent={
            <Button
              onPress={() => this.props.navigation.navigate('pages')}
              title="zpet"
              buttonStyle={{
                width: 80,
                height: 5,
                backgroundColor: '#673AB7'
              }}
            />
          }
          centerComponent={{
            text:
              this.props.pageData == null ? 'Informace o stránce' : this.props.pageData.data.name,
            style: { color: '#fff' }
          }}
          outerContainerStyles={{ backgroundColor: '#673AB7' }}
        />
        {this.renderContent()}
      </View>
    );
  }
}

function mapStateToProps({ PagesReducer, InsightsReducer }) {
  return {
    pageData: PagesReducer.pageData,
    error: PagesReducer.error,
    timesToPost: PagesReducer.timesToPost,
    insightsData: InsightsReducer.insightsData
  };
}

const styles = {
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 10
  }
};

export default connect(mapStateToProps, actions)(AnalyticsScreen);
