import { TabNavigator } from 'react-navigation';

import AnalyticsScreen from '../screens/AnalyticsScreen';
import AuthScreen from '../screens/AuthScreen';
import PagesScreen from '../screens/PagesScreen';
import WelcomeScreen from '../screens/WelcomeScreen';

const MainNavigator = TabNavigator(
  {
    welcome: { screen: WelcomeScreen },
    auth: { screen: AuthScreen },
    main: {
      screen: TabNavigator(
        {
          pages: { screen: PagesScreen },
          analytics: { screen: AnalyticsScreen }
        },
        {
          navigationOptions: {
            tabBarVisible: false
          }
        }
      )
    }
  },
  {
    lazy: true,
    navigationOptions: {
      tabBarVisible: false
    }
  }
);

export default MainNavigator;
