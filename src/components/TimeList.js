import React from 'react';
import { Text } from 'react-native';
import { Card, List, ListItem } from 'react-native-elements';

const TimeList = ({ data }) => {
  if (data) {
    return (
      <Card>
        <Text>Recommended posting times for today</Text>
        <List>{data.map(item => <ListItem key={item} title={item} />)}</List>
      </Card>
    );
  }
  return (
    <Card>
      <Text>Could not retrieve best times for your post!</Text>
    </Card>
  );
};

export default TimeList;
