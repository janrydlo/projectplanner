import React from 'react';
import { Text } from 'react-native';
import { Card } from 'react-native-elements';

import PageListItem from './PageListItem';

const PageList = ({ pages, callback }) => {
  if (pages) {
    return pages.map(item => {
      const { picture, data } = item;
      return <PageListItem key={data.id} data={data} image={picture} callback={callback} />;
    });
  }
  return (
    <Card>
      <Text>You are not managing any pages!</Text>
    </Card>
  );
};

export default PageList;
