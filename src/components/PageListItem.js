import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Card } from 'react-native-elements';

const PageListItem = ({ data, image, callback }) => {
  const { thumbnailContainerStyle, thumbnailStyle, contentStyle } = styles;

  if (data) {
    const { id, name, about } = data;

    return (
      <TouchableOpacity onPress={() => callback(id)}>
        <Card key={id} title={name}>
          <View style={thumbnailContainerStyle}>
            <Image style={thumbnailStyle} source={{ uri: image }} />
            <View style={contentStyle}>
              <Text>{about}</Text>
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
  return (
    <Card>
      <Text>Could not retrieve data from this page!</Text>
    </Card>
  );
};

const styles = {
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around'
  },
  headerTextStyle: {
    fontSize: 18
  },
  thumbnailStyle: {
    height: 50,
    width: 50
  },
  thumbnailContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  }
};

export default PageListItem;
