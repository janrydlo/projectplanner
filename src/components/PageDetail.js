import React from 'react';
import { View, Text } from 'react-native';
import { Card, Avatar, Icon } from 'react-native-elements';

const PageDetail = ({ data: pageData }) => {
  const { profilePictureStyle, infoItemsStyle, descriptionStyle } = styles;
  if (pageData) {
    const {
      picture,
      data: { website },
      data: { engagement: { count: likeCount } },
      data: { about: description }
    } = pageData;

    return (
      <View>
        <Card>
          <View style={profilePictureStyle}>
            <Avatar xlarge source={{ uri: picture }} />
          </View>
          <View style={infoItemsStyle}>
            <Icon name="thumb-up" />
            <Text>{likeCount}</Text>
            <Icon name="link" />
            <Text>{website == null ? 'N/A' : website}</Text>
          </View>
        </Card>
        <View style={descriptionStyle}>
          <Card>
            <Text>
              {description == null
                ? 'There is no description for this page!'
                : description.length > 110 ? `${description.substring(0, 109)}...` : description}
            </Text>
          </Card>
        </View>
      </View>
    );
  }
  return (
    <Card>
      <View style={descriptionStyle}>
        <Text>Could not get info about your page!</Text>
      </View>
    </Card>
  );
};

const styles = {
  profilePictureStyle: {
    alignItems: 'center'
  },
  descriptionStyle: {
    alignItems: 'stretch'
  },
  infoItemsStyle: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 20
  }
};

export default PageDetail;
