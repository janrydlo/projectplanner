import {
  PAGES_FETCH_SUCCESS,
  FETCH_FAIL,
  PAGE_DATA_FETCH_SUCCESS,
  TIMES_COUNTING_SUCCESS
} from '../actions/types';

export const INITIAL_STATE = {
  pages: null,
  error: null,
  pageData: null,
  timesToPost: null
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PAGES_FETCH_SUCCESS:
      return { ...state, pages: action.payload };
    case FETCH_FAIL:
      return { ...state, error: action.payload };
    case PAGE_DATA_FETCH_SUCCESS:
      return { ...state, error: null, pageData: action.payload };
    case TIMES_COUNTING_SUCCESS:
      return { ...state, error: null, timesToPost: action.payload };
    default:
      return state;
  }
};
