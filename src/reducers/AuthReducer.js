import { FACEBOOK_LOGIN_SUCCESS, FACEBOOK_LOGIN_FAIL } from '../actions/types';

export const INITIAL_STATE = {
  token: null,
  error: null
};

export default (state = INITIAL_STATE, action) => {
  console.log('Auth reducer');
  switch (action.type) {
    case FACEBOOK_LOGIN_SUCCESS:
      return { ...state, error: null, token: action.payload };
    case FACEBOOK_LOGIN_FAIL:
      return { ...state, error: action.payload };
    default:
      return state;
  }
};
