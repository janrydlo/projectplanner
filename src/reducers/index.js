import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import PagesReducer from './PagesReducer';
import InsightsReducer from './InsightsReducer';

export default combineReducers({ AuthReducer, PagesReducer, InsightsReducer });
