import { INSIGHTS_DATA_FETCH_FAIL, INSIGHTS_DATA_FETCH_SUCCESS } from '../actions/types';

export const INITIAL_STATE = { insightsData: null, error: null };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case INSIGHTS_DATA_FETCH_SUCCESS:
      return { ...state, insightsData: action.payload };
    case INSIGHTS_DATA_FETCH_FAIL:
      return { ...state, error: action.error };
    default:
      return state;
  }
};
