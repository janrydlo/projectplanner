import ct from 'countries-and-timezones';

const COMMENT_WEIGHT = 10;
const REACTION_WEIGHT = 1;
const SHARE_WEIGHT = 20;
const PEOPLE_ONLINE_WEIGHT = 1;
const DAILY_POST_NUMBER = 5;
const TIMES_ON_PAGE = 3;
const PERCENTAGE_DIFFERENCE_THRESHOLD = 10;

//http://coschedule.com/blog/best-times-to-post-on-social-media
const DEFAULT_TIMES = ['9:OO', '13:00', '15:00'];

const TIMES_KIDS = ['7', '8', '15', '16', '17', '18', '19'];
const TIMES_ADULTS = ['6', '7', '17', '18', '19', '20', '21'];

const UTC_PST_DIFFERENCE = -8;

export const getBestDaysForPosts = (insightsData, postsData, todaysPosts) => {
  try {
    const {
      data: {
        data: [{ values: hoursValues }, { values: genderAgeValues }, { values: countryValues }]
      }
    } = insightsData;
    const { data: { data: postData } } = postsData;
    const { data: { data: todaysPostsData } } = todaysPosts;

    const genderAgeDataMap = getGenderAgeFrequenciesMap(genderAgeValues);
    const popularHoursDataMap = getOnlineHoursFrequenciesMap(hoursValues);
    const countriesDataMap = getCountryFrequenciesMap(countryValues);
    const postsHistoryDataMap = getPostsHistoryData(postData);
    const timeZoneOffset = getTimezoneOffset(countriesDataMap);
    const isAdults = isAudienceAdults(genderAgeDataMap);

    const result = mergeAnalyticsData(popularHoursDataMap, postsHistoryDataMap).sort((x, y) =>
      sortHoursByMetrics(x, y, isAdults, timeZoneOffset)
    );

    const resultWithoutPassedHours = removePassedHours(result, timeZoneOffset);
    //const resultWithoutPassedHours = result;

    const recommendedTimes = createOutputArray(
      resultWithoutPassedHours,
      todaysPostsData.length,
      timeZoneOffset
    );
    return recommendedTimes;
  } catch (e) {
    return DEFAULT_TIMES;
  }
};

const getGenderAgeFrequenciesMap = genderAgeValues => getFrequencyMap(genderAgeValues);
const getOnlineHoursFrequenciesMap = hoursValues => getFrequencyMap(hoursValues);
const getCountryFrequenciesMap = countryValues => getFrequencyMap(countryValues);

const getFrequencyMap = arrayOfValues => {
  const frequencyMap = new Map();

  arrayOfValues.forEach(item => {
    if (item.value) {
      Object.entries(item.value).forEach(([key, value]) => {
        const frequencyValue = frequencyMap.get(key);
        if (frequencyValue) {
          frequencyMap.set(key, frequencyValue + value);
        } else {
          frequencyMap.set(key, value);
        }
      });
    }
  });
  return frequencyMap;
};

const getTimezoneOffset = countriesData => {
  const mostPopularCountry = Array.from(countriesData.keys()).reduce(
    (x, y) => (countriesData.get(x) > countriesData.get(y) ? x : y)
  );
  const mxTimezones = ct.getTimezonesForCountry(mostPopularCountry);
  const timeZoneDifference = mxTimezones[0].utcOffset / 60;
  return UTC_PST_DIFFERENCE + -timeZoneDifference;
};

const isAudienceAdults = genderAgeFrequenciesMap => {
  const mostPopularAge = Array.from(genderAgeFrequenciesMap.keys()).reduce(
    (x, y) => (genderAgeFrequenciesMap.get(x) > genderAgeFrequenciesMap.get(y) ? x : y)
  );
  return Number(mostPopularAge.substring(2, 4)) >= 18;
};

const getPostsHistoryData = postData => {
  const frequencyMap = new Map();
  postData.forEach(item => {
    let time = item.created_time.split('T')[1].substring(0, 2);
    if (time.startsWith(0)) {
      time = time.substring(1, 2);
    }
    // nutno prevest cas CET-2 na PST, aby sedely mapy
    const timeCet = shiftHourBy(Number(time), +2);
    const timePst = shiftHourBy(Number(timeCet), -9);
    const timeStr = `${timePst}`;
    const valueObject = frequencyMap.get(timeStr);
    if (valueObject) {
      frequencyMap.set(timeStr, {
        comments: valueObject.comments + item.comments.summary.total_count,
        reactions: valueObject.reactions + item.reactions.summary.total_count,
        shares: valueObject.shares + item.shares ? item.shares.count : 0,
        reach: valueObject.reach + item.insights.data[0].values[0].value
      });
    } else {
      frequencyMap.set(timeStr, {
        comments: item.comments.summary.total_count,
        reactions: item.reactions.summary.total_count,
        shares: item.shares ? item.shares.count : 0,
        reach: item.insights.data[0].values[0].value
      });
    }
  });
  return frequencyMap;
};

const mergeAnalyticsData = (peopleOnlineMap, postsDataMap) => {
  peopleOnlineMap.forEach((value, key) => {
    peopleOnlineMap.set(key, {
      comments: 0,
      reactions: 0,
      shares: 0,
      reach: 0,
      peopleOnline: value
    });
  });
  postsDataMap.forEach((value, key) => {
    const mapValue = peopleOnlineMap.get(key);
    peopleOnlineMap.set(key, { ...mapValue, ...value });
  });

  return Array.from(peopleOnlineMap);
};

const removePassedHours = (array, timeZoneDifference) => {
  const currentHour = new Date().getHours();
  const newArray = array.filter(
    item => shiftHourBy(Number(item[0]), -timeZoneDifference) > currentHour
  );
  return newArray;
};

const createOutputArray = (arrayWithoutPassedHours, numberOfTodaysPosts, timeZoneOffset) => {
  if (numberOfTodaysPosts >= DAILY_POST_NUMBER) {
    return [];
  }
  const resultArray = [];
  for (
    let i = 0;
    i <
    Math.min(
      DAILY_POST_NUMBER - numberOfTodaysPosts,
      TIMES_ON_PAGE,
      arrayWithoutPassedHours.length
    );
    i++
  ) {
    const time = shiftHourBy(Number(arrayWithoutPassedHours[i][0]), -timeZoneOffset);
    resultArray.push(`${time}:OO`);
  }
  resultArray.sort();
  return resultArray;
};

const sortHoursByMetrics = (x, y, isAdults, timeZoneOffset) => {
  let weightX = x[1].reach;
  let weightY = y[1].reach;

  if (getPercentageDifference(weightX, weightY) < PERCENTAGE_DIFFERENCE_THRESHOLD) {
    const timeX = shiftHourBy(Number(x[0]), -timeZoneOffset);
    const timeY = shiftHourBy(Number(y[0]), -timeZoneOffset);
    const rightTimeForAgeBonusX = isBonusAllowed(`${timeX}`, isAdults) ? 1.5 : 1;
    const rightTimeForAgeBonusY = isBonusAllowed(`${timeY}`, isAdults) ? 1.5 : 1;

    weightX =
      (x[1].comments * COMMENT_WEIGHT +
        x[1].reactions * REACTION_WEIGHT +
        x[1].shares * SHARE_WEIGHT +
        x[1].peopleOnline * PEOPLE_ONLINE_WEIGHT) *
      rightTimeForAgeBonusX;
    weightY =
      (y[1].comments * COMMENT_WEIGHT +
        y[1].reactions * REACTION_WEIGHT +
        y[1].shares * SHARE_WEIGHT +
        y[1].peopleOnline * PEOPLE_ONLINE_WEIGHT) *
      rightTimeForAgeBonusY;
  }
  return weightY - weightX;
};

const getPercentageDifference = (x, y) => Math.abs(x - y) / ((x + y) / 2) * 100;

const shiftHourBy = (hour, by) => {
  const shiftedHour = hour + by;
  if (shiftedHour > 23) {
    return shiftedHour - 23;
  }
  if (shiftedHour < 0) {
    return shiftedHour + 24;
  }
  return shiftedHour;
};

const isBonusAllowed = (time, isAdults) => {
  if (isAdults) {
    return TIMES_ADULTS.includes(time);
  }
  return TIMES_KIDS.includes(time);
};
